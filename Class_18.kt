package com.example.firstkotlin

fun main(args: Array<String>) {

}

abstract class Cricket{
    abstract var player_name:String
    abstract var number_of_matches:Int
    abstract var runs:Int

    abstract fun update()
    abstract fun write_to()
    abstract fun Display()
    abstract fun Search()
    abstract fun Fifties()
    abstract fun Hundreds()
    abstract fun Wickets()
    abstract fun Overs()

    fun subha(){

    }

}

class Bat(override var player_name: String,
          override var number_of_matches: Int,
          override var runs: Int
) :Cricket(){
    override fun update(){

    }

    override fun write_to() {
        TODO("Not yet implemented")
    }

    override fun Display() {
        TODO("Not yet implemented")
    }

    override fun Search() {
        TODO("Not yet implemented")
    }

    override fun Fifties() {
        TODO("Not yet implemented")
    }

    override fun Hundreds() {
        TODO("Not yet implemented")
    }

    override fun Wickets() {
        TODO("Not yet implemented")
    }

    override fun Overs() {
        TODO("Not yet implemented")
    }
}

abstract class Bowler:Cricket(){

}
