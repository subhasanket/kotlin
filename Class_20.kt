package com.example.firstkotlin

class Player(runs:Int, number_of_matches:Int){
    var runs = runs
    var number_of_matches = number_of_matches

    fun add():Int{
        return this.runs+this.number_of_matches
    }
}

fun main() {
    var player1 = Player(20,10)
    var player2 = Player(30,10)
    println(player1.add())
    println(player2.add())
}