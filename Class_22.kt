package com.example.firstkotlin

open class  Book{
    val isbn:String by lazy { "isbn" }
    val title:String by lazy { "deposite" }
    val price:Double by lazy { 2.0 }
}

class Magazine(isbn:String,title: String,price:Double) : Book() {
    val magazine :Magazine by lazy { magazine }
}

class Novel(s: String, s1: String, d: Double) : Book() {
    val author:Novel by lazy { author }
}

fun main() {
    var magazine = Magazine("hello","depo",2.2)
    var novel = Novel("hello","depo",2.2)
    println(novel.isbn)
}