fun main(args: Array<String>) {
	var arr:IntArray = intArrayOf(1, 0, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0)
	var res=runs(arr)
	println(res)
}

fun runs(a: IntArray): Int {
    var res = 0;
    if (a.size == res)
        return res

    var base = a[res]
    a.forEach({
        if(it != base){
            res ++
            base = it
        }
    })

    return (++res);   
}