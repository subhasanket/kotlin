package com.example.firstkotlin

class  Bank(var depositorName: String,var accountNo : Int,var accountType : String,var amount : Int){

    fun  deposit(dAmmount:Int){
        this.amount += dAmmount
    }

    fun withdraw(wAmmount:Int){
        this.amount-=wAmmount
    }

    fun details(){
        println("depositor: ${depositorName}\naccountNo: ${accountNo}\naccontType: ${accountType}\namount: ${amount}")
    }
}

fun main() {
    var ram = Bank("Ram",123342121,"Savings",0)
    ram.deposit(20)
    ram.details()

    var sham = Bank("Sham",123342121,"Savings",0)
    var asish = Bank("Asish",123342121,"Savings",0)
    var subha = Bank("Subha",123342121,"Savings",0)
}