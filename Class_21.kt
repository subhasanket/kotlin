package com.example.firstkotlin

import java.util.*

open  class Payment(var payment: Int){


    open fun paymentDetails(){
        println("Amount of payment ${payment}")
    }
}

class  CashPayment(payment: Int): Payment(payment) {
    override  fun paymentDetails(){
        println("Amount of payment ${payment}")
    }
}

class CreditCardPayment(payment: Int) :Payment(payment){
    var cardName:String = ""
    var expDate:String =""
    var creditCardNo:Int = 0

    constructor(cardName: String,expDate: String,creditCardNo:Int,payment: Int):this(payment)
    {
        this.cardName=cardName
        this.expDate=expDate
        this.creditCardNo=creditCardNo
    }
}

fun main() {
    var cashPayment1 = CashPayment(300)
    var cashPayment2 = CashPayment(200)
    var creditCardPayment = CreditCardPayment(2001)
    var creditCardPayment2 = CreditCardPayment(400)
    cashPayment1.paymentDetails()
    cashPayment2.paymentDetails()
    creditCardPayment.paymentDetails()
    creditCardPayment2.paymentDetails()
}